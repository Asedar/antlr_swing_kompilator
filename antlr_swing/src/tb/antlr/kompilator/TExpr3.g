tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer blockIdentifier = 0; 
}
prog    : (e+=currentScope | e+=expr | d+=decl)* -> progTemplate(name={$e},declarations={$d});

currentScope : ^(LB {blockIdentifier++; localSymbols.enterScope();} (e+=currentScope | e+=expr | d+=decl)* {blockIdentifier--; localSymbols.leaveScope();}) -> blockTemplate(expression={$e}, declarations={$d});

decl  :
        ^(VAR i1=ID) {localSymbols.newSymbol($ID.text + blockIdentifier.toString());} -> dek(n={$ID.text + blockIdentifier.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                                                                 -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                                                                 -> sub(p1={$e1.st},p2={$e2.st}) 
        | ^(MUL   e1=expr e2=expr)                                                                 -> mul(p1={$e1.st},p2={$e2.st}) 
        | ^(DIV   e1=expr e2=expr)                                                                 -> div(p1={$e1.st},p2={$e2.st}) 
        | ^(PODST i1=ID   e2=expr) {checkIfVariableExists($i1.text + blockIdentifier.toString());} -> set(p1={$i1.text + blockIdentifier.toString()},p2={$e2.st})
        | ID {checkIfVariableExists($ID.text + blockIdentifier.toString());}                       -> get(p1={($ID.text + blockIdentifier.toString())})
        | INT  {numer++;}                                                                          -> int(i={$INT.text})
    ;
    